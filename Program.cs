﻿using System;

namespace TaskAbstract
{
    interface IOffice
    {
        string OfficeAddress { get; set; }
        string Remote();
    }

    interface ITime : IOffice
    {
        string PartFull();
        int HoursPerMonth { get; set; }
    }
    interface IPosition : ITime
    {
        string Department { get; set; }
        int SubordinateNumber { get; set; }
        string Project { get; set; }
        int PayPerHour { get; set; }
        int Salary();
    }
    interface ITerms : IPosition { }

    interface IPrintMe
    { 
        void PrintMe();
    }


    class Company : IPrintMe
    {
        public string companyName = "Epam";
        public string address = "Kyiv";
        public string country = "Ukraine";
        public int numberEmployees = 0;
        public string CompanyName
        { 
            get { return companyName; } 
            set { companyName = value; } 
        }
        public string Address
        { 
            get { return address; } 
            set { address = value; } 
        }
        public string Country 
        { 
            get { return country; } 
            set { country = value; } 
        }
        public int NumberEmployees
        {
            get { return numberEmployees; }
        }

        public void PrintMe()
        {
            Console.WriteLine(
                $"CompanyName: { companyName } \n" +
                $"Address: {address}\n" +
                $"Country: = {country} \n" +
                $"Number of Employees {numberEmployees} \n"
                );
        }
    }

    abstract class Employee : Company
    {
        abstract public string Name { get; set; }
        abstract public string LastName { get; set; }
        public Employee()
        { 
            numberEmployees++; 
        }
    }


    class Manager : Employee, ITerms, IPrintMe
    {
        public string name;
        public string lastName;
        public string officeAddress;
        int hoursPerMonth = 160;
        public override string Name
        {
            get { return name; }
            set { name = value; }
        }
        public override string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }
        public string OfficeAddress
        {
            get { return officeAddress; }
            set { officeAddress = value; }
        }
        public string Remote()
        {
            return officeAddress == null ? "Remote" : "Office";
        }
        public string PartFull()
        {
            return hoursPerMonth < 160 ? "Part Time" : "Full Time";
        }
        public int HoursPerMonth
        {
            get { return hoursPerMonth; }
            set { hoursPerMonth = value; }
        }
        public string Department { get; set; }
        public int SubordinateNumber { get; set; }
        public string Project { get; set; }
        public int payPerHour = 0;
        public int PayPerHour
        {
            get { return payPerHour; }
            set { payPerHour = value; }
        }
        public int Salary()
        {
            return payPerHour * hoursPerMonth;
        }
        public new void PrintMe()
        {
            Console.WriteLine(
                $"Name: {name} \n" +
                $"Last Name: {lastName} \n" +
                $"Office address: {officeAddress} \n" +
                $"Remote or not: {Remote()}\n" +
                $"Part/Full time: = {PartFull()} \n" +
                $"Hours Per Month: {hoursPerMonth} \n" +
                $"Department: {Department} \n" +
                $"Number of Subordinates: {SubordinateNumber} \n" +
                $"Project: {Project} \n" +
                $"Pay per hour ($): {payPerHour} \n" +
                $"Hours per month: {HoursPerMonth} \n" +
                $"Salary ($) {Salary()} \n"
                );
        }
    }


    internal class Program
    {
        static void Main(string[] args)
        {
            Company microsoft = new Company();
            Manager jack = new Manager();
            Manager eva = new Manager();
            jack.Name = "Jack";
            jack.LastName = "Black";
            jack.officeAddress = "Haidara";
            jack.payPerHour = 20;
            jack.SubordinateNumber = 4;
            jack.Department = "Sales";
            jack.Project = "54FGR";
            jack.PrintMe();

            microsoft.companyName = "Microsoft";
            microsoft.address = "San Francisco";
            microsoft.country = "USA";
            microsoft.PrintMe();
        }
    }
}

